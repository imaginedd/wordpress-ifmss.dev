<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<footer id="colophon" class="grid_lock site-footer" role="contentinfo">
	<div class="site-info">
		<?php
			/**
			 * Fires before the twentysixteen footer text for footer customization.
			 *
			 * @since Twenty Sixteen 1.0
			 */
			do_action( 'twentysixteen_credits' );
		?>
		<span class="copyright"><?php printf( __('%s', 'ifmss'), '&copy;' ); ?> <?= date('Y'); ?></span>
		<span class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
		<span class="rights-reserved"><?php _e('All rights reserved', 'ifmss'); ?>
	</div><!-- .site-info -->
</footer><!-- .site-footer -->

<?php wp_footer(); ?>
</body>
</html>
