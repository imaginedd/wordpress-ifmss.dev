/*
 * Gulp File
 * @subpackage IFMSS
 * @author Edd Hurst <eddhurst.co.uk>
 * @since 1.0.2
*/
// Load Dependencies
var gulp = require('gulp'),
  sass = require('gulp-ruby-sass'),
  autoprefixer = require('autoprefixer'),
  postcss = require('gulp-postcss'),
  cleancss = require('gulp-clean-css'),
  jshint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  gutil = require('gulp-util'),
  imagemin = require('gulp-imagemin'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  cache = require('gulp-cache'),
  notify = require('gulp-notify')

// Process our Sass Files
gulp.task('sass', function () {
  return sass('assets/scss/style.scss', { style: 'expanded' })
    .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
		.pipe(cleancss())
		.pipe(gulp.dest('dist/css'))
		.pipe(notify({message:'Sass completed'}))
})

// Scripts
gulp.task('scripts', function () {
  return gulp.src([
    'assets/js/*.js',
    'assets/js/vendor/*.js'
  ])
		.pipe(concat('scripts.js'))
		.pipe(rename({ suffix: '.min' }))
		.pipe(jshint())
		.pipe(uglify().on('error', gutil.log))
		.pipe(gulp.dest('dist/js'))
		.pipe(notify({message:'Scripts completed'}))
})

// Compress Images
gulp.task('images', function () {
  return gulp.src('assets/images/*')
  .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
  .pipe(gulp.dest('dist/images'))
  .pipe(notify({message:'Image compression completed'}))
})

// Set default task order
gulp.task('default', function () {
  gulp.start('sass', 'scripts', 'images', 'watch')
})

// Watch for changing files
gulp.task('watch', function () {
	// Watch Sass files
  gulp.watch('assets/scss/**/*.scss', ['sass'])

	// Watch JS files
  gulp.watch('assets/js/**/*.js', ['scripts'])

	// Watch image files
  gulp.watch('assets/images/**/*', ['images'])
})
