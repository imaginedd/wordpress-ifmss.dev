<?php

// Enqueuing scripts
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {

	$parent_style = 'twentysixteen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

	// Parent theme styles
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	
	// Child theme default stylesheet
	wp_enqueue_style( 'child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version')
	);

	// Sass theme stylesheet
	wp_enqueue_style( 'sass', get_stylesheet_directory_uri() . '/dist/css/style.css' );
}

// Segregate the function files to their component parts, keep it a bit neater
add_action( 'init', 'child_theme_enqueue_function_segments' );
function child_theme_enqueue_function_segments(){

	require_once( 'inc/functions/custom_post_types.php' );
	require_once( 'inc/functions/woocommerce.php' );

}

// Function to change sender name
function ifmss_sender_name( $original_email_from ) {
	return 'IFMSS';
}

// Hooking up our functions to WordPress filters 
add_filter( 'wp_mail_from_name', 'ifmss_sender_name' );


?>