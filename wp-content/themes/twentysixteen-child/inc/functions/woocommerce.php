<?php

// Isolating WooCommerce functions into separate functions file to keep everything neat



// Add a post excerpt onto the product archive page
add_action( 'woocommerce_after_shop_loop_item_title', 'output_product_excerpt', 35 );
function output_product_excerpt() {
	global $post;
	echo '<div class="short_description">';
		echo $post->post_excerpt;
	echo '</div>';
}

// Hide Product count when showing categories
add_filter( 'woocommerce_subcategory_count_html', 'jk_hide_category_count' );
function jk_hide_category_count() {
	return false;
}

// Prevent showing the subcategory thumbnail
if ( ! function_exists( 'woocommerce_subcategory_thumbnail' ) ) {
	function woocommerce_subcategory_thumbnail( $category ) {
		return false;
	}
}


?>