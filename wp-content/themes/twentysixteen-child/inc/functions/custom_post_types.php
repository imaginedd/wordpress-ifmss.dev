<?php 

// let's create the function for the custom type
function custom_post_example() { 
	// creating (registering) the custom type 
	register_post_type( 'classes', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Classes', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Class', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Classes', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Class', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Classes', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Class', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Class', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Classes', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the example custom post type', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-welcome-learn-more', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'class', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'class', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
		) /* end of options */
	); /* end of register post type */
	
}

// adding the function to the Wordpress init
add_action( 'init', 'custom_post_example');
	
// now let's add custom categories (these act like categories)
register_taxonomy( 'course', 
	array('classes'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Course', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Course', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Course', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Courses', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Course', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Course:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Course', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Course', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Course', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Course Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'course' ),
	)
);

?>