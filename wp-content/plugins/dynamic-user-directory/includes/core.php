<?php
function DynamicUserDirectory( )
{
	
global $wpdb;
global $userid;
$plugins = get_option('active_plugins');
$dud_options = get_option( 'dud_plugin_settings' );

/*** Load the scripts ***/
if (!wp_script_is( 'user-directory-style', 'enqueued' )) {
  
	wp_register_style('user-directory-style',  DYNAMIC_USER_DIRECTORY_URL . '/css/user-directory-min.css', false, 0.1);	
	wp_enqueue_style( 'user-directory-style' );
	//wp_register_style('user-directory-style',  DYNAMIC_USER_DIRECTORY_URL . '/css/user-directory.css', false, 0.1);	
	//wp_enqueue_style( 'user-directory-style' );
	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'); 
}

/*** Turn DEBUG on if user has admin privileges and debug setting is set to "on" ***/
global $dynamic_ud_debug; 
$dynamic_ud_debug = false;

if(current_user_can('administrator'))
	if($dud_options['ud_debug_mode'] === "on")
		$dynamic_ud_debug = true;
		
if($dynamic_ud_debug)
	dynamic_ud_dump_settings();
		
/*** Get sort, hide roles, search, and include/exclude fields ***/
$user_directory_sort = $dud_options['user_directory_sort'];
$ud_hide_roles = $dud_options['ud_hide_roles'];
$exc_inc_radio = $dud_options['ud_exclude_include_radio'];
$inc_exc_user_ids = $dud_options['ud_users_exclude_include'];
$ud_directory_type = $dud_options['ud_directory_type'];
$ud_show_srch = $dud_options['ud_show_srch'];

/*** Get the search field or search letter ***/
$dud_user_srch_key = $_REQUEST ["dud_user_srch_key"]; //For the meta flds srch add-on

if(is_null($dud_user_srch_key) || $dud_user_srch_key === "") 
	$dud_user_srch_key = '';

$dud_user_srch_name = $_REQUEST ["dud_user_srch_val"];

if(is_null($dud_user_srch_name) || $dud_user_srch_name === "") 
	$dud_user_srch_name = '';

/*** Load an array with alphabet letters corresponding to existing user last names ***/

if ( ! in_array( 'dynamic-user-directory-meta-flds-srch/dud_meta_flds_srch.php' , $plugins ) || !$dud_user_srch_name || !$ud_show_srch)
	$letters = dynamic_ud_load_alpha_links($user_directory_sort, $ud_hide_roles, $exc_inc_radio, $inc_exc_user_ids);

else	
{	
	$letters = apply_filters('dud_meta_fld_srch_load_alpha_links', $letters, $user_directory_sort, $ud_hide_roles, $exc_inc_radio, $inc_exc_user_ids, $dud_user_srch_key, $dud_user_srch_name);
	
	//Meta field search came up empty
	if(count($letters) == 0 && $dud_user_srch_name ) 
	{	
		$letters = dynamic_ud_load_alpha_links($user_directory_sort, $ud_hide_roles, $exc_inc_radio, $inc_exc_user_ids);
		$srch_err = "<H3>No users were found matching your search criteria.</H3>";
	}
}


/* For developers who want to modify the plugin */
$letters = apply_filters( 'dud_after_load_letters', $letters);

if ( count($letters) == 0 && !$dud_user_srch_name )	return "<H3>There are no users in the directory at this time.</H3>";

if ( ! in_array( 'dynamic-user-directory-meta-flds-srch/dud_meta_flds_srch.php' , $plugins ) || !$ud_show_srch) 
{
	if(strlen($dud_user_srch_name) > 0) {

		if(strlen($dud_user_srch_name) > 50)
			return "<H3>The search field is limited to 50 characters!</H3>";
				
		$dud_user_srch_name = sanitize_text_field($dud_user_srch_name);
		
		$last_name_letter = substr($dud_user_srch_name, 0, 1); 
		
	}
	else
	{
		$last_name_letter = $_REQUEST ["letter"];

		if(is_null($last_name_letter) || $last_name_letter === "") 
			$last_name_letter = $letters[0];
	}	
}
else
{	
	if(strlen($dud_user_srch_name) > 0) {

		if(strlen($dud_user_srch_name) > 50)
			return "<H3>The search field is limited to 50 characters!</H3>";
				
		$dud_user_srch_name = sanitize_text_field($dud_user_srch_name);
	}
	
	$last_name_letter = $_REQUEST ["letter"];
	
	if(is_null($last_name_letter) || $last_name_letter === "") 
		$last_name_letter = $letters[0];
}

/*** Validate request data to protect against SQL injection ***/
if(!ctype_alpha($last_name_letter) || strlen($last_name_letter) > 1) 
	return "<H3>There are no users in the directory at this time.</H3>";	

/*** SQL QUERY ****************************************************************/

$roles_sql = "";
$include_exclude_sql = "";

/* For Meta Fields Search Add-On Only: If a search value was entered and the results should be shown on a single page */
if(in_array( 'dynamic-user-directory-meta-flds-srch/dud_meta_flds_srch.php' , $plugins ) &&
		$dud_options['ud_show_srch_results'] === 'single-page' && $dud_user_srch_name)
		{
			$dud_options['ud_letter_divider'] = "ld-fl";		
			$ud_directory_type = "all-users";
		}	
				
if ( ! in_array( 'dynamic-user-directory-meta-flds-srch/dud_meta_flds_srch.php' , $plugins ) || !$dud_user_srch_name || !$ud_show_srch) 
{	
	if($ud_hide_roles && !($exc_inc_radio === 'include' && $inc_exc_user_ids))
		$roles_sql = dynamic_ud_build_roles_query($user_directory_sort, $ud_hide_roles);
		
	if($inc_exc_user_ids)
		$include_exclude_sql = dynamic_ud_build_inc_exc_query($user_directory_sort, $ud_hide_roles, $exc_inc_radio, $inc_exc_user_ids);	

	if($user_directory_sort === "last_name") 
	{    
		if(!($ud_directory_type === "all-users"))
			$user_sql = "SELECT DISTINCT user_id from " . $wpdb->prefix 
				. "usermeta WHERE meta_key = 'last_name' and meta_value like '" . $last_name_letter . "%' ";
		else
			$user_sql = "SELECT DISTINCT user_id from " . $wpdb->prefix . "usermeta WHERE meta_key = 'last_name'";
		
		if($ud_hide_roles && !($exc_inc_radio === 'include' && $inc_exc_user_ids)) 
			$user_sql .= " AND " . $roles_sql;
			
		if($inc_exc_user_ids) 
			$user_sql .= " AND " . $include_exclude_sql;
			
		$user_sql .= " ORDER BY meta_value"; 
	}		
	else
	{ 
		if(!($ud_directory_type === "all-users"))
			$user_sql = "SELECT DISTINCT ID, display_name from " . $wpdb->prefix . "users WHERE display_name like '" . $last_name_letter . "%'" ;
		else
			$user_sql = "SELECT DISTINCT ID, display_name from " . $wpdb->prefix . "users" ;
		
		if($ud_hide_roles && !($exc_inc_radio === 'include' && $inc_exc_user_ids)) 
		{     
			if($ud_directory_type === "all-users")
				$user_sql .= " WHERE " . $roles_sql;
			else
				$user_sql .= " AND " . $roles_sql;
		}
		if($inc_exc_user_ids) 
		{
			if($ud_directory_type === "all-users" && !($ud_hide_roles && !($exc_inc_radio === 'include' && $inc_exc_user_ids))) 
				$user_sql .= " WHERE " . $include_exclude_sql;
			else
				$user_sql .= " AND " . $include_exclude_sql;
		}

		$user_sql .= " ORDER BY display_name"; 	
	}	
}
else 
	$user_sql = apply_filters( 'dud_meta_fld_srch_build_sql', $user_sql, $last_name_letter, $dud_user_srch_key, $dud_user_srch_name);

$uids = $wpdb->get_results($user_sql);

/* For developers who want to modify the plugin */
$uids = apply_filters( 'dud_after_load_uids', $uids);

if($dynamic_ud_debug) { echo "<PRE>Load Users SQL:<BR><BR>" . $user_sql . "<BR><BR></PRE>"; }

/*** If users were found ***/
if($uids)
{      
	$inc = 1;
	$listing_cnt = 0;
	$user_fullname = "";
	$user_website = ""; 
	$user_first_name = "";
	$user_last_name = ""; 
	$user_approval = ""; 
	$user_avatar_url = "";
	$last_name_letter = "";
	$printed_letter = "";
	
	/*** OPTION SETTINGS ******************************************************************/
	
	/*** Display Preferences ***/
    $user_directory_show_avatar = $dud_options['user_directory_show_avatars'];	     
    $user_directory_avatar_style = $dud_options['user_directory_avatar_style'];      
	$user_directory_border = $dud_options['user_directory_border'];
	$user_directory_border_length = $dud_options['user_directory_border_length'];
	$user_directory_border_style = $dud_options['user_directory_border_style'];
	$user_directory_border_color = $dud_options['user_directory_border_color'];
	$user_directory_border_thickness = $dud_options['user_directory_border_thickness'];
	$user_directory_listing_fs = $dud_options['user_directory_listing_fs'];
	$user_directory_listing_sp = $dud_options['user_directory_listing_spacing'];
	$ud_author_page = $dud_options['ud_author_page'];
	$ud_show_author_link = $dud_options['ud_show_author_link'];
	$ud_target_window = $dud_options['ud_target_window'];
	$ud_letter_divider = $dud_options['ud_letter_divider'];
	$ud_letter_divider_font_color = $dud_options['ud_letter_divider_font_color'];
	$ud_letter_divider_fill_color = $dud_options['ud_letter_divider_fill_color'];
	$letter_div_shadow = ""; 
	if($ud_letter_divider === "ld-ds") $letter_div_shadow = " letter-div-shadow";
	$ud_srch_style = $dud_options['ud_srch_style'];
	$ud_format_name = $dud_options['ud_format_name'];
	$sort_order_items = dynamic_ud_sort_order( $dud_options['user_directory_sort_order'] );
	
	if($ud_directory_type === "all-users" && $ud_letter_divider !== "nld") {
		if(!($user_directory_border === "surrounding_border" || $user_directory_border === "dividing_border"))
			$user_directory_border_length = "65%";
	}
	
	/* For developers who want to modify the sort order */
	$sort_order_items = apply_filters( 'dud_after_load_sort_order', $sort_order_items);
	
	/*** Meta field names, keys, and labels ***/
	$user_directory_addr_1_op = $dud_options['user_directory_addr_1'];
	$user_directory_addr_2_op = $dud_options['user_directory_addr_2'];
	$user_directory_city_op = $dud_options['user_directory_city'];
	$user_directory_state_op = $dud_options['user_directory_state'];
	$user_directory_zip_op = $dud_options['user_directory_zip'];
	$user_directory_meta_flds = array();

	$fldIdx = 0;
	for ($inc=0; $inc<10; $inc++)
	{
		$tmp_fld = $dud_options['user_directory_meta_field_' . ($inc+1)];
		
		if($tmp_fld) 
		{
			$user_directory_meta_flds[$fldIdx] = array();
			$user_directory_meta_flds[$fldIdx]['field'] = $tmp_fld;
			$user_directory_meta_flds[$fldIdx]['label'] = $dud_options['user_directory_meta_label_' . ($inc+1)];
			$user_directory_meta_flds[$fldIdx]['key'] = "MetaKey" . ($inc+1);
			$user_directory_meta_flds[$fldIdx]['link'] = $dud_options['user_directory_meta_link_' . ($inc+1)];
			$fldIdx++;
		}	
		else
		{
			$idx = array_search( ("MetaKey" . ($inc+1) ), $sort_order_items);
			
			if($idx===false) continue;
			else unset($sort_order_items[$idx]); //if meta key has empty value, remove from sort list
		}
	}
			
	/* For developers who want to modify the plugin */
	$user_directory_meta_flds = apply_filters('dud_after_load_meta_flds', $user_directory_meta_flds);
	
	/*** Meta fields from wp_users table ***/
	$user_directory_email = $dud_options['user_directory_email'];      //wp_users field
	$user_directory_website = $dud_options['user_directory_website'];  //wp_users field
	
	/*** Set defaults for empty options ***/
	if(!$user_directory_border_length) $user_directory_border_length = "100%";
	if(!$user_directory_border_style) $user_directory_border_style = "solid";
	if(!$user_directory_border_color) $user_directory_border_color = "#dddddd";
	if(!$user_directory_border_thickness) $user_directory_border_thickness = "1px";
	if(!$user_directory_listing_fs) $user_directory_listing_fs = "12px";
	if(!$user_directory_listing_sp) $user_directory_listing_sp = "20";
	if(!$ud_format_name) $ud_format_name = "fl";
																	
	/*** DISPLAY DIRECTORY ********************************************************************************/
								
	/*** Display the alphabet links at the top ***/
	if(!($ud_directory_type === "all-users"))
	{
		/*** For the meta fld search add-on ***/
		if ( in_array( 'dynamic-user-directory-meta-flds-srch/dud_meta_flds_srch.php' , $plugins ) && $ud_show_srch) 
			$alpha_links = apply_filters('dud_meta_fld_srch_print_alpha_links', $letters, $dud_options['ud_alpha_link_spacer'], $dud_options['user_directory_letter_fs'], $dud_user_srch_key, $dud_user_srch_name) . "<BR>";
		else 
			$alpha_links = dynamic_ud_print_alpha_links($letters, $dud_options['ud_alpha_link_spacer'], $dud_options['user_directory_letter_fs']) . "<BR>";
		
		$user_contact_info .= $alpha_links;
	}
	else
	{
	    /* For Alpha Links Scroll add-on */
		$user_contact_info .= apply_filters( 'dud_print_scroll_letter_links', $user_contact_info, $letters);
	}	
	
	/*** Search box stuff ***/	
	if($ud_show_srch)
	{
		$user_srch_placeholder_txt = "Last Name";
	
		if($user_directory_sort !== "last_name")
			$user_srch_placeholder_txt = "Display Name";
	
		if($ud_srch_style === "transparent") $ud_srch_style = "background:none;";
		else $ud_srch_style = "";
	        	
        $user_directory_srch_fld = "<form id='dud_user_srch' method='post'>\n"; 
		$user_directory_srch_fld .= "    \t<DIV id='user-srch' style='width:45%;'>\n";
       	$user_directory_srch_fld .= "          \t\t<input type='text' id='dud_user_srch_val' name='dud_user_srch_val' style='"  . $ud_srch_style . "'"; 
        $user_directory_srch_fld .= " value='' maxlength='50' placeholder='" . $user_srch_placeholder_txt . "'/>\n";
        $user_directory_srch_fld .= "        \t\t<button type='submit' id='dud_user_srch_submit' name='dud_user_srch_submit' value=''>\n";
        $user_directory_srch_fld .= "             \t\t\t<i class='fa fa-search fa-lg' aria-hidden='true'></i>\n";
        $user_directory_srch_fld .= "        \t\t</button>\n";
		$user_directory_srch_fld .= "     \t</DIV>\n";
        $user_directory_srch_fld .="</form><BR>\n"; 
		
		$user_directory_srch_fld = apply_filters('dud_build_srch_form', $user_directory_srch_fld);
		
		if($srch_err)
		{
			$user_contact_info .= $user_directory_srch_fld . $srch_err;
			//echo "returning search err <BR>";
			return $user_contact_info;
		}
		else
			$user_contact_info .= $user_directory_srch_fld;
    }
	
	/*** Determine if Cimy User Extra Fields plugin is installed and active ***/
	if ( in_array( 'cimy-user-extra-fields/cimy_user_extra_fields.php' , $plugins ) ) 
		$user_directory_cimy = TRUE;  //installed & active
	else
		$user_directory_cimy = FALSE; //not installed or inactive
	
	/*** So we don't print a dividing border after the last listing for a given letter ***/
	$last_listing_uid = dynamic_ud_last_listing($uids, $user_directory_sort);
	
	/*** Loop through all users with last name or display name matching the selected letter ***/
	foreach ($uids as $uid)
	{      
	    $user_id = 0;
		$user_directory_csz = "";
		$user_fullname = "";
		$user_website = ""; 
		$user_first_name = "";
		$user_last_name = ""; 
		$user_directory_addr_1 = "";
		$user_directory_addr_2 = "";
		$user_directory_city = "";
		$user_directory_state = "";
		$user_directory_zip = "";
		$cimy_avatar_loc = "";
		$got_cimy_data = false;
		
		//Remove old meta fld values from the previous iteration
		unset($user_directory_meta_flds_tmp);
		$user_directory_meta_flds_tmp = $user_directory_meta_flds;
		
		/*** Gather the data ******************************************/	
		
		if($user_directory_sort === "last_name")
		{
			$user_id = $uid->user_id;
			$user_last_name = get_user_meta($user_id, 'last_name', true);
		}
		else
		{
			$user_id = $uid->ID; 
			$user_last_name = $uid->display_name;
		}
		
		if ( ! in_array( 'dynamic-user-directory-meta-flds-srch/dud_meta_flds_srch.php' , $plugins ) ) 
		{
			if($dud_user_srch_name)
			{ 
				 if (!(strpos(strtoupper ($user_last_name), strtoupper ($dud_user_srch_name)) === 0))
				 {	         
					  continue;
				 }	 
			}
		}
		
		$ud_author_posts = count_user_posts( $user_id ); //used to see if we should link to the WP author page
				           				 		
		if($user_directory_cimy)  //Cimy meta fields
		{
			$values = dynamic_ud_get_cimy_data($user_id, $user_directory_addr_1_op, $user_directory_addr_2_op, $user_directory_city_op,
	 			$user_directory_state_op, $user_directory_zip_op, $user_directory_meta_flds_tmp);
	 					
			if($values) 
			{
				foreach ($values as $value)
				{ 
				 	$meta_name = strtoupper ($value->NAME);
				 	
				 	if($value->TYPE === 'avatar') 
						$cimy_avatar_loc = $value->VALUE;	
				 	
					else if($user_directory_addr_1_op && $meta_name === strtoupper ($user_directory_addr_1_op)) 
						$user_directory_addr_1 = $value->VALUE;	
						 
					else if($user_directory_addr_2_op && $meta_name === strtoupper ($user_directory_addr_2_op)) 
						$user_directory_addr_2 = $value->VALUE;	
						  
					else if($user_directory_city_op && $meta_name === strtoupper ($user_directory_city_op)) 
						$user_directory_city = $value->VALUE;	
						 
					else if($user_directory_state_op && $meta_name === strtoupper ($user_directory_state_op)) 
						$user_directory_state = $value->VALUE;	
						 
					else if($user_directory_zip_op && $meta_name === strtoupper ($user_directory_zip_op)) 
						$user_directory_zip = $value->VALUE;	
					else
					{					
						for($inc=0; $inc < sizeof($user_directory_meta_flds_tmp); $inc++ ) 
						{
							if($user_directory_meta_flds_tmp[$inc]['field'] 
								&& $meta_name === strtoupper ($user_directory_meta_flds_tmp[$inc]['field'])) 
							{
								//currently Cimy doesn't appear to store arrays but we are ready if/when it does
								$user_directory_meta_flds_tmp[$inc]['value'] =  dynamic_ud_parse_meta_val($value->VALUE);
								break;
							}						
						}
					}
										
					$got_cimy_data = true;				 
				}			
			}	
		}
		if(!$user_directory_cimy || ($user_directory_cimy && !$got_cimy_data))  //wp_usermeta fields
		{	 
			if($user_directory_addr_1_op) $user_directory_addr_1 = get_user_meta($user_id, $user_directory_addr_1_op, true);	 
			if($user_directory_addr_2_op) $user_directory_addr_2 = get_user_meta($user_id, $user_directory_addr_2_op, true); 
			if($user_directory_city_op) $user_directory_city = get_user_meta($user_id, $user_directory_city_op, true); 
			if($user_directory_state_op) $user_directory_state = get_user_meta($user_id, $user_directory_state_op, true); 
			if($user_directory_zip_op) $user_directory_zip = get_user_meta($user_id, $user_directory_zip_op, true);
			
			for($inc=0; $inc < sizeof($user_directory_meta_flds_tmp); $inc++) 
			{		  
			   if($user_directory_meta_flds_tmp[$inc]['field']) 
			   {
				   //calling get_user_meta() this way so that we can parse meta fields than contain arrays
				   $user_meta_fld = get_user_meta($user_id, $user_directory_meta_flds_tmp[$inc]['field']);	
				   
				   $user_meta_fld = $user_meta_fld[0]; //it will always be an array even for single values
					
				   $user_directory_meta_flds_tmp[$inc]['value'] =  dynamic_ud_parse_meta_val($user_meta_fld);
				}
			} 			
		}
		
		/* For developers who want to modify the plugin */
		$user_directory_meta_flds_tmp = apply_filters('dud_after_load_meta_vals', $user_directory_meta_flds_tmp, $user_id);
	
		if($dynamic_ud_debug) {
			echo "<PRE>";
			echo "Meta Flds for User: " . $user_last_name . "<BR><BR>";
		
			for($inc=0; $inc < sizeof($user_directory_meta_flds_tmp); $inc++ ) 
			{
				echo "&nbsp;&nbsp;&nbsp;Fld: " . $user_directory_meta_flds_tmp[$inc]['field'] . "&nbsp;&nbsp;";	
				echo "Lbl: " . $user_directory_meta_flds_tmp[$inc]['label'] . "&nbsp;&nbsp;";	
				echo "Key: " . $user_directory_meta_flds_tmp[$inc]['key'] . "&nbsp;&nbsp;";	
				echo "Val: " . $user_directory_meta_flds_tmp[$inc]['value'] . "<BR>";	
				echo "Link: " . $user_directory_meta_flds_tmp[$inc]['link'] . "<BR>";				
			}
			echo "<BR></PRE>";
		}
		
		$userdata = get_userdata($user_id);	//wp_users fields
		
		if($user_directory_website)
			$user_website =  $userdata->user_url;
		if($user_directory_email)
			$user_email =  $userdata->user_email;	
			
		$username = $userdata->user_login;      // For cimy plugin - may not be needed		
					
		/*** Prepare the data ****************************************/
		
		if($user_directory_city && $user_directory_state && $user_directory_zip)
		    $user_directory_csz = $user_directory_city . ", " . $user_directory_state . " " . $user_directory_zip;	
		else if($user_directory_city && $user_directory_state)
		    $user_directory_csz = $user_directory_city . ", " . $user_directory_state;	
		else
		{        
		        if($user_directory_city)
		             $user_directory_csz .= $user_directory_city . " ";	
		        if($user_directory_state)
		             $user_directory_csz .= $user_directory_state . " ";	
		        if($user_directory_zip)
		             $user_directory_csz .= $user_directory_zip;	
		}
		    
		if($user_directory_sort === "last_name")	
		{
			$user_first_name = get_user_meta($user_id, 'first_name', true);	
      		$user_fullname = "<b>" . $user_first_name . " " . $user_last_name . "</b>";
      			
      		if($ud_format_name === "lf")
      		{
      			$user_fullname = "<b>" . $user_last_name . ", " . $user_first_name . "</b>";
      		}
      			
      		/* For developers who want to modify the plugin */
			$user_fullname = apply_filters('dud_set_user_full_name', $user_fullname, $user_first_name, $user_last_name);
      	}
      	else
      	{
      		$user_fullname = "<b>". $uid->display_name . "</b>";
      	} 
      		
      	if($ud_author_page && ($ud_author_posts > 0 || $ud_show_author_link === "always" ))
      	{     				
      		$user_fullname_tmp = "<a href=\"" . get_author_posts_url( get_the_author_meta( 'ID', $user_id ), 
      			get_the_author_meta( 'user_nicename', $user_id ) ) . "\"";
      		
      		if($ud_target_window === "separate") $user_fullname_tmp .= " target='_blank'";
      			
      		$user_fullname_tmp .= ">" . $user_fullname . "</a>";
      		
      		$user_fullname = $user_fullname_tmp;
      	}    		
					
		/*** Print the data ****************************************/
		
		if($ud_directory_type === "all-users" && $ud_letter_divider !== "nld")
		{
			if(strtoupper($printed_letter) !== strtoupper(substr($user_last_name, 0, 1)))
			{
				$printed_letter = substr($user_last_name, 0, 1);
				
				$user_contact_info .= "\n<DIV style=\"width:" . $user_directory_border_length 
					. "; background-color: " . $ud_letter_divider_fill_color 
						. ";\" class=\"printed-letter-div" . $letter_div_shadow;
				
				$user_contact_info .= "\"><DIV id=\"letter-divider-" . $printed_letter . "\" style=\"color:" . $ud_letter_divider_font_color 
					. ";\" class=\"printed-letter\">" . $printed_letter . "</DIV></DIV>";					
			}
		}
		
		if($user_directory_border === "surrounding_border")
		{
			$user_contact_info .= "\n<DIV style=\"width:" . $user_directory_border_length . "; border-style:" 
				. $user_directory_border_style . ";border-width:" . $user_directory_border_thickness . ";border-color:" 
					. $user_directory_border_color . ";\" class=\"dir-listing-border\" >";
			
		}
		
		$user_contact_info .= "\n<DIV class=\"dir-listing\">\n";	
					
		/*** Print Avatar ***/		
	    if($user_directory_show_avatar)
	    {          	
	       	if($user_directory_avatar_style === "rounded-edges")
	       	{
	       	 	$atts = array('class' => 'avatar-rounded-edges');
	       	 	$img_style = "avatar-rounded-edges";
	       	}
        	else if($user_directory_avatar_style === "circle")
        	{
               	$atts = array('class' => 'avatar-circle');
              	$img_style  = "avatar-circle";
            }
            else
            {
              	$atts = array('class' => '');
               	$img_style  = "";
            }
                         	
            if($user_directory_cimy)
               	$user_avatar_url = dynamic_ud_get_cimy_avatar($user_id, $username, $atts, $img_style, $cimy_avatar_loc );
            else
           		$user_avatar_url = get_avatar( $user_id, '', '', '', $atts );
                       		
           	if($ud_author_page && ($ud_author_posts > 0 || $ud_show_author_link))
      		{     				
      			$user_avatar_url_tmp = "<a href=\"" . get_author_posts_url( get_the_author_meta( 'ID', $user_id ), 
      				get_the_author_meta( 'user_nicename', $user_id ) ) . "\"";
      		
      			if($ud_target_window === "separate") $user_avatar_url_tmp .= " target='_blank'";
      				
      			$user_avatar_url_tmp .= ">" . $user_avatar_url . "</a>";
      		
      			$user_avatar_url = $user_avatar_url_tmp;
      		} 
								   	
			if($user_directory_avatar_style === "circle")
				$user_contact_info .= "\t<DIV class='user-avatar-circle'>". $user_avatar_url . "</DIV>\n\t";
			else
				$user_contact_info .= "\t<DIV class='user-avatar'>". $user_avatar_url . "</DIV>\n\t";
			
			if($user_directory_border === "surrounding_border")
				$user_contact_info .= "<DIV style='font-size:" . $user_directory_listing_fs . "px;' class='dir-listing-text-surr-border'>\n\t\t";
			else
				$user_contact_info .= "<DIV style='font-size:" . $user_directory_listing_fs . "px;' class='dir-listing-text'>\n\t\t";
		}
		else	
			$user_contact_info .= "\n\t<DIV style='font-size:" 
				. $user_directory_listing_fs . "px;' class='dir-listing-text-no-avatar'>\n\t\t";
			
		/*** Sort Field field is always displayed first ***/
		
		if($user_fullname !== '')
			$user_contact_info .= "\t" . $user_fullname . "<br>\n";
			 
		/*** Print remaining fields in the chosen display order ***/	 
		$line_cnt = 0;
	
		foreach ($sort_order_items as $item)
		{
			if($item === "Email")
			{
				if($user_directory_email && $user_email !== '') {
					$user_contact_info .= "\t\t\t<a href=\"mailto:" . $user_email . "\" target=\"_top\">" . $user_email . "</a><br>\n";	
					$line_cnt++;
				}
			}
			else if($item === "Website")
			{
				if($user_directory_website && $user_website !== '') {
					$user_contact_info .= "\t\t\t<a href=\"" . $user_website . "\">" . $user_website . "</a><br>\n";	
					$line_cnt++;
				}
			}
			else if($item === "Address")
			{
				if($user_directory_addr_1) { $user_contact_info .= "\t\t\t" . $user_directory_addr_1 . "<br>\n"; $line_cnt++; }
				if($user_directory_addr_2) { $user_contact_info .= "\t\t\t" . $user_directory_addr_2 . "<br>\n"; $line_cnt++; }
				if($user_directory_csz) { $user_contact_info .= "\t\t\t" .$user_directory_csz . "<br>\n"; $line_cnt++; }
			}
			else
			{
				foreach ( $user_directory_meta_flds_tmp as $ud_mflds )
				{
					if($item === $ud_mflds['key'])			
					{									
						if($ud_mflds['value'] && $ud_mflds['label']) 
							$user_contact_info .= "\t\t\t<b>" . $ud_mflds['label'] . " </b>\n";
							
						if($ud_mflds['value']) {
							if($ud_mflds['link'] === '#')
								$user_contact_info .= "\t\t\t<a href=\"" . $ud_mflds['value'] . "\">" . $ud_mflds['value'] . "</a><br>\n";	
							else
								$user_contact_info .= "\t\t\t" . $ud_mflds['value'] . "<br>\n"; 
							$line_cnt++; 
							break;
						}	
					}
				}
			}
		}
				 	
		/*** Close the proper divs and print the dividing border if that is being used ***/					
		$user_contact_info .= "\t</DIV>\n</DIV>\n";	
										
		if($user_directory_border === "surrounding_border")
			$user_contact_info .= "</DIV>\n"; 	
					
		else if($user_directory_border === "dividing_border" && ($user_id !== $last_listing_uid))
		{
			$user_contact_info .= "<DIV style=\"width:" . $user_directory_border_length 
			    . ";border-style:" . $user_directory_border_style . ";border-width:" . $user_directory_border_thickness . ";border-color:" . 	
			         $user_directory_border_color . ";\" class=\"dir-listing-border-2\" ></DIV>\n";
		}
			
		/* space between each listing */			
		$user_contact_info .= "<DIV style=\"height:" . $user_directory_listing_sp . "px;\"></DIV>";	
		
		$listing_cnt++;
	  			
	} //END foreach ($uids as $uid)
	
	if($dud_user_srch_name && $listing_cnt < 1)
		$user_contact_info .= "<H3>No users were found matching your search criteria.</H3>";	
	
	if($listing_cnt > 8 && !($ud_directory_type === "all-users"))
	{
		/*** Display the alphabet links at the top ***/
		if ( !in_array( 'dynamic-user-directory-meta-flds-srch/dud_meta_flds_srch.php' , $plugins ) 
				&& !in_array( 'dynamic-user-directory-alpha-links-scroll/dynamic-user-directory-alpha-links-scroll.php' , $plugins )) 
					$user_contact_info .=  "<BR>" . dynamic_ud_print_alpha_links($letters, $dud_options['ud_alpha_link_spacer'], $dud_options['user_directory_letter_fs']) . "<BR>";
		
	}
	
	return $user_contact_info;
}
else
{	
	if($dud_user_srch_name)
	{
		if(!($ud_directory_type === "all-users"))
		{
			/*** For the meta fld search add-on ***/
			if ( in_array( 'dynamic-user-directory-meta-flds-srch/dud_meta_flds_srch.php' , $plugins ) ) 
				$alpha_links = apply_filters('dud_meta_fld_srch_print_alpha_links', $letters, $dud_options['ud_alpha_link_spacer'], $dud_options['user_directory_letter_fs'], $dud_user_srch_key, $dud_user_srch_name) . "<BR>";
			else 
				$alpha_links = dynamic_ud_print_alpha_links($letters, $dud_options['ud_alpha_link_spacer'], $dud_options['user_directory_letter_fs']) . "<BR>";
			
			$user_contact_info .= $alpha_links;
		}
				
		$user_srch_placeholder_txt = "Last Name";
	
		if($user_directory_sort !== "last_name")
			$user_srch_placeholder_txt = "Display Name";
	
		if($ud_srch_style === "transparent") $ud_srch_style = "background:none;";
		else $ud_srch_style = "";
	        	
        $user_directory_srch_fld = "<form id='dud_user_srch' method='post'>\n"; 
		$user_directory_srch_fld .= "    \t<DIV id='user-srch' style='width:" . $user_directory_border_length . ";'>\n";
       	$user_directory_srch_fld .= "          \t\t<input type='text' id='dud_user_srch_val' name='dud_user_srch_val' style='"  . $ud_srch_style . "'"; 
        $user_directory_srch_fld .= " value='' maxlength='50' placeholder='" . $user_srch_placeholder_txt . "'/>\n";
        $user_directory_srch_fld .= "        \t\t<button type='submit' id='dud_user_srch_submit' name='dud_user_srch_submit' value=''>\n";
        $user_directory_srch_fld .= "             \t\t\t<i class='fa fa-search fa-lg' aria-hidden='true'></i>\n";
        $user_directory_srch_fld .= "        \t\t</button>\n";
		$user_directory_srch_fld .= "     \t</DIV>\n";
        $user_directory_srch_fld .="</form><BR>\n"; 
		
		$user_directory_srch_fld = apply_filters('dud_build_srch_form', $user_directory_srch_fld);
		$user_contact_info .= $user_directory_srch_fld;
		
		if($srch_err)
			$user_contact_info .= $srch_err;
		
		return $user_contact_info;
		
	}
	else
		return "<H3>There are no users in the directory at this time.</H3>";	
}
	
}
add_shortcode( 'DynamicUserDirectory', 'DynamicUserDirectory' );

//** DUD UTILITY FUNCTIONS **************************************************************************************/

/*** Format meta fields that contain an array of items. If nested arrays are encountered, dump the contents  ***/
function dynamic_ud_parse_meta_val($user_meta_fld)
{
	$parsed_val = "";
	
	if(is_array($user_meta_fld))
	{
		foreach ($user_meta_fld as $key => $value) 
		{
			if (is_string($key))
			{	
				if(is_array($value)) //there are nested arrays
					$parsed_val .= "<BR>" . var_export($value, true);	
				else                 //add key-value pair to the meta fld var
					$parsed_val .= "<BR> " . $key . ": " . $value;
			} 
			else
			{
				$numeric_idx = true;
				break;
			}
		}
		
		if($numeric_idx)
		{
			for($met=0; $met < sizeof($user_meta_fld); $met++) 
			{
				if($user_meta_fld[$met])
				{
					if(is_array($user_meta_fld[$met])) //there are nested arrays
						$parsed_val .= var_export($user_meta_fld[$met], true);
					else                               //add the item to the meta fld var
						$parsed_val .= "<BR> " . $user_meta_fld[$met];
				}
			}
		}
		
		return $parsed_val;
	}
	
	return $user_meta_fld;	
}

/*** Returns the user id of the last listing for a selected letter so that the dividing border is not printed after it. ***/
function dynamic_ud_last_listing($uids, $user_directory_sort)
{
	$last_listing_uid = 0;
	
	foreach ($uids as $uid)
	{
		if($user_directory_sort === "last_name")
			$last_listing_uid  = $uid->user_id;		
		else
			$last_listing_uid  = $uid->ID;	
	}
	
	return $last_listing_uid;
}

function dynamic_ud_sort_order( $input ) {
       
     $output = "";
        
     if($input) 
         $output = explode(',', $input);
         
     else
     {
     	$output = "Address,Email,Website,MetaKey1,MetaKey2,MetaKey3,MetaKey4,MetaKey5,MetaKey6,MetaKey7,MetaKey8,MetaKey9,MetaKey10";
     	$output = explode(',', $output);
     }
     
     return $output;
}

/*** Loads an array with the alphabet letters for the existing users based on the filters selected on the settings page  ***/
function dynamic_ud_load_alpha_links($sort_fld, $ud_hide_roles, $exc_inc_radio, $inc_exc_user_ids)
{
	global $dynamic_ud_debug;
	global $wpdb;
	$roles_sql = "";
	$include_exclude_sql = "";
	
	if($ud_hide_roles && !($inc_exc_user_ids && $exc_inc_radio === 'include' )) //if including users, no need to build hide roles query
		$roles_sql = dynamic_ud_build_roles_query($sort_fld, $ud_hide_roles);
		
	if($inc_exc_user_ids)
		$include_exclude_sql = dynamic_ud_build_inc_exc_query($sort_fld, $ud_hide_roles, $exc_inc_radio, $inc_exc_user_ids);	
	
	
	if($sort_fld === "last_name")
	{
		$ud_sql = "Select COUNT(*) as cnt,SUBSTRING(meta_value,1,1) as letter FROM " . $wpdb->prefix . "usermeta where meta_key = 'last_name' ";
		
		if($ud_hide_roles && !($inc_exc_user_ids && $exc_inc_radio === 'include' )) $ud_sql .= " AND " . $roles_sql; 
		
		if($inc_exc_user_ids) $ud_sql .= " AND " . $include_exclude_sql;
			
		$ud_sql .= " GROUP BY SUBSTRING(meta_value,1,1)";
	}
	else
	{
		$ud_sql = "Select COUNT(*) as cnt, SUBSTRING(display_name,1,1) as letter FROM " . $wpdb->prefix . "users ";
		
		if( ($ud_hide_roles && !($inc_exc_user_ids && $exc_inc_radio === 'include' )) || $inc_exc_user_ids) $ud_sql .= " where ";
	
		if($ud_hide_roles && !($inc_exc_user_ids && $exc_inc_radio === 'include' )) $ud_sql .= $roles_sql;
		
		if($inc_exc_user_ids)
		{
			if($ud_hide_roles && !($inc_exc_user_ids && $exc_inc_radio === 'include' )) $ud_sql .= " AND ";
			
		 	$ud_sql .= $include_exclude_sql; 
		}
	
		$ud_sql .= " GROUP BY SUBSTRING(display_name,1,1)";
		
		if($dynamic_ud_debug) { echo "<PRE>Load Alpha Links SQL:<BR><BR>" . $ud_sql . "<BR><BR></PRE>"; }
	}
		
	$results = $wpdb->get_results($ud_sql);
	
	if($results)
	{	
		if($dynamic_ud_debug) { echo "<PRE>Existing Users Sorted By Letter<BR>"; }
		
		$letter_exists = array();
	
		foreach($results as $result) {
			if($dynamic_ud_debug) { echo strtoupper($result->letter) . "&nbsp;&nbsp;(" . $result->cnt . ")"; }
			
			if(ctype_alpha($result->letter)) 
			{
				if($dynamic_ud_debug) echo " *";
				array_push($letter_exists, strtoupper($result->letter));
			}
			if($dynamic_ud_debug) echo "<BR>";
		}
		if($dynamic_ud_debug) echo "</PRE>";
			
		return $letter_exists;
	}

	return null; 
}

/***  Prints the letters of alphabet as links that will be used by the MembersListing function ***/
function dynamic_ud_print_alpha_links($ud_existing_letters, $ud_alpha_link_spacer, $user_directory_letter_fs)
{
	global $wp;
	global $dynamic_ud_debug;
			
	if(!$user_directory_letter_fs) $user_directory_letter_fs = "14px";
		
	if(!$ud_alpha_link_spacer) $ud_alpha_link_spacer = "8px";
	else $ud_alpha_link_spacer .= "px";
		
	$current_url = esc_url( home_url( '/' ) ) . basename(get_permalink());
	
	$url_param = "/?";
	
	if ((strpos($current_url, "?") !== false)) $url_param = "&";
	        
	$ud_letters_links = "\n<DIV class=\"alpha-links\" style=\"font-size:" . $user_directory_letter_fs . "px;\">\n";

	/*** alphabet array ***/
		
	$ud_alpha_string = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
	$ud_alpha_array = explode(',', $ud_alpha_string);
		
	if($dynamic_ud_debug) echo "<PRE>";	
	for( $inc = 0; $inc<26; $inc++ )
	{	
	    	$ud_letter = $ud_alpha_array[$inc];
	    	
	    	if($dynamic_ud_debug) echo "Checking letter " . $ud_letter;	
	    	
		if(in_array ( $ud_letter, $ud_existing_letters ))
		{	
			if($dynamic_ud_debug) echo " *";			
			if($ud_letter !== 'Z')
				$ud_letters_links .= "\t\t<a href=\"" . $current_url . $url_param . "letter=" . $ud_letter . "\"><b>" . $ud_letter . "</b></a><span style=\"padding-right:" 
					. $ud_alpha_link_spacer . ";\"></span>\n";
			else
				$ud_letters_links .= "\t\t<a style=\"font-weight: 400;\" href=\"" 
					. $current_url . $url_param . "letter=" . $ud_letter . "\"><b>" . $ud_letter . "</b></a>\n";
		}
		else
		{
			if($ud_letter !== 'Z')
				$ud_letters_links .= "\t\t<span style=\"color:gray;padding-right:" 
					. $ud_alpha_link_spacer . ";\">". $ud_letter . "</span>\n";
			else
				$ud_letters_links .= "\t\t<span style=\"color:gray\">". $ud_letter . "</span>\n";
		}
		if($dynamic_ud_debug) echo "<BR>";	
	}

	if($dynamic_ud_debug) echo "</PRE>";	
	$ud_letters_links .= "</DIV>\n\n";
	
	return $ud_letters_links;
}

/*** Cimy Utilities ***********************************************************************************/

function dynamic_ud_get_cimy_avatar($id, $user_login, $atts, $img_style, $cimy_avatar_loc )
{
	if (isset($id)) 
	{			
		$dud_avatar_file_path = dynamic_ud_before_last ('/', $cimy_avatar_loc);
		
		$dud_avatar_file_abs_path = dynamic_ud_between_last ('wp-content', '/', $cimy_avatar_loc);
		
		$dud_avatar_file_name = dynamic_ud_between_last ('/', '.', $cimy_avatar_loc);
		
		$dud_avatar_file_ext = dynamic_ud_after_last ('.', $cimy_avatar_loc);
		
		$dud_avatar_thumb_abs_path = ABSPATH . "wp-content" . $dud_avatar_file_abs_path . "/" . $dud_avatar_file_name . "-thumbnail." . $dud_avatar_file_ext;
		
		$dud_avatar_thumb_path = $dud_avatar_file_path . "/" . $dud_avatar_file_name . "-thumbnail." . $dud_avatar_file_ext;
				
		if ($cimy_avatar_loc) 
		{					
			if(file_exists($dud_avatar_thumb_abs_path)) //use the thumbnail if it exists for quicker load
				return "<img alt='' src='{$dud_avatar_thumb_path}' class='avatar " . $img_style  . "' height='96px' width='96px' />";
			else
				return "<img alt='' src='{$cimy_avatar_loc}' class='avatar " . $img_style  . "' height='96px' width='96px' />";
		}
		else 
			return get_avatar($id, '', '', '', $atts);
	}
}

/*** Builds Cimy Tables Query Based on Dynamic User Directory Key Name Settings ***/
function dynamic_ud_get_cimy_data($id, $user_directory_addr_1_op, $user_directory_addr_2_op, $user_directory_city_op,
	 			$user_directory_state_op, $user_directory_zip_op, $user_directory_meta_flds_tmp)
{
	global $wpdb;
	global $dynamic_ud_debug;
	
	$ud_sql = "SELECT data.VALUE, efields.NAME, efields.TYPE FROM " . DUD_CIMY_DATA_TABLE . " as data JOIN " . DUD_CIMY_FIELDS_TABLE . 
			" as efields ON efields.id=data.field_id WHERE (";
	
	$was_prev_fld = 0;
	$values = null;
			
	if($user_directory_addr_1_op) { $ud_sql .= "efields.NAME='". $user_directory_addr_1_op . "'"; $was_prev_fld = 1; }
	if($user_directory_addr_2_op) { if($was_prev_fld) { $ud_sql .= " OR "; } $ud_sql .= "efields.NAME='" . $user_directory_addr_2_op . "'"; $was_prev_fld = 1; }
	if($user_directory_city_op) { if($was_prev_fld) { $ud_sql .= " OR "; } $ud_sql .= "efields.NAME='" . $user_directory_city_op . "'"; $was_prev_fld = 1; }
	if($user_directory_state_op) { if($was_prev_fld) { $ud_sql .= " OR "; } $ud_sql .= "efields.NAME='" . $user_directory_state_op . "'"; $was_prev_fld = 1; }
	if($user_directory_zip_op) { if($was_prev_fld) { $ud_sql .= " OR "; } $ud_sql .= "efields.NAME='" . $user_directory_zip_op . "'"; $was_prev_fld = 1; }
		
	foreach ( $user_directory_meta_flds_tmp as $ud_mflds )
	{
		if($ud_mflds['field']) { 
			if($was_prev_fld) { $ud_sql .= " OR "; } 
			$ud_sql .= "efields.NAME='" . $ud_mflds['field'] . "'"; 
			$was_prev_fld = 1; 
		}
	}
	
	if($was_prev_fld) { $ud_sql .= " OR "; } $ud_sql .= "efields.TYPE='avatar'";
	
	$ud_sql .= ") AND data.USER_ID = " . $id;
	
	if($dynamic_ud_debug) { echo "<PRE>Cimy SQL:<BR><BR>" . $ud_sql . "<BR><BR></PRE>"; }
			
	return  $wpdb->get_results($ud_sql);
}

/*** SQL Utilities ***/

function dynamic_ud_build_roles_query($sort_fld, $ud_hide_roles) {

	$roles_sql = "";
	$role_cnt = 1;
	$user_id_col_name = "ID";
	global $wpdb;
	
	if($sort_fld === "last_name") $user_id_col_name = "user_id"; 
		
	$roles_sql .= $user_id_col_name . " NOT IN (SELECT user_id FROM " . $wpdb->prefix . "usermeta WHERE ((
		meta_key = '" . $wpdb->prefix . "capabilities' AND (";
			
	$roles_arr_len = count($ud_hide_roles);
	
	foreach($ud_hide_roles as $role)
	{
		$roles_sql .= " meta_value like '%" . $role . "%'";
		
		if($role_cnt < $roles_arr_len)
			$roles_sql .= " OR ";
		
		$role_cnt++;
	}	
	
	$roles_sql .= ")))) ";
	
	return $roles_sql;
}

function dynamic_ud_build_inc_exc_query($sort_fld, $ud_hide_roles, $exc_inc_radio, $inc_exc_user_ids) {

	$users_sql = "";
	$user_cnt = 1;
	$user_id_col_name = "ID";
	global $wpdb;
		
	if($sort_fld === "last_name") $user_id_col_name = "user_id";
	
	$users_arr_len = count($inc_exc_user_ids);
	
	if($exc_inc_radio === "include") {
		
		$users_sql .= "( ";	
		
		foreach($inc_exc_user_ids as $user_id)
		{
			$users_sql .= $user_id_col_name . " = " . $user_id;
		
			if($user_cnt < $users_arr_len)
				$users_sql .= " OR ";
		
			$user_cnt++;
		}	
		
		$users_sql .= " ) ";
	}
	else if($exc_inc_radio === "exclude") {
		
		$users_sql .= "(" . $user_id_col_name . " NOT IN (SELECT user_id FROM " . $wpdb->prefix . "usermeta WHERE ( ";	
		
		foreach($inc_exc_user_ids as $user_id)
		{
			$users_sql .= $user_id_col_name . " = " . $user_id;
		
			if($user_cnt < $users_arr_len)
				$users_sql .= " OR ";
		
			$user_cnt++;
		}	
		
		$users_sql .= " ))) ";
	}
	
	return $users_sql;
}

/*** String Utilities ***/

function dynamic_ud_before ($instr, $inthat)
{
        return substr($inthat, 0, strpos($inthat, $instr));
};

function dynamic_ud_after ($instr, $inthat)
{
        if (!is_bool(strpos($inthat, $instr)))
        return substr($inthat, strpos($inthat,$instr)+strlen($instr));
};
        
function dynamic_ud_after_last ($instr, $inthat)
{
        if (!is_bool(dynamic_ud_strrevpos($inthat, $instr)))
        	return substr($inthat, dynamic_ud_strrevpos($inthat, $instr)+strlen($instr));
}
    
function dynamic_ud_before_last ($instr, $inthat)
{
        return substr($inthat, 0, dynamic_ud_strrevpos($inthat, $instr));
}

function dynamic_ud_between_last ($instr, $that, $inthat)
{
        return dynamic_ud_after_last($instr, dynamic_ud_before_last($that, $inthat));
}    

function dynamic_ud_strrevpos($instr, $needle)
{
    $rev_pos = strpos (strrev($instr), strrev($needle));
    if ($rev_pos===false) return false;
    else return strlen($instr) - $rev_pos - strlen($needle);
};

function endswith($string, $test) {
    $strlen = strlen($string);
    $testlen = strlen($test);
    if ($testlen > $strlen) return false;
    return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
}

/*** Debug Utility ***/
function dynamic_ud_dump_settings()
{
	global $wpdb;
	
	$dud_options = get_option( 'dud_plugin_settings' );
	
	echo "<PRE>";
	
	if(defined("DUD_CIMY_DATA_TABLE") && defined("DUD_CIMY_FIELDS_TABLE")) 
	{
		$plugins = get_option('active_plugins');
		
		if ( in_array( 'cimy-user-extra-fields/cimy_user_extra_fields.php' , $plugins ) ) {
			$dynamic_ud_cimy_installed = "INSTALLED AND ACTIVE"; 
		}
		else
			$dynamic_ud_cimy_installed = "INSTALLED BUT INACTIVE"; 
    }  	     
    else
       	$dynamic_ud_cimy_installed = "NOT INSTALLED OR INACTIVE"; 
     	    	
    echo "Users Table Name: " . $wpdb->prefix . "users " . "<BR><BR>";
    	
    echo "User Meta Table Name: " . $wpdb->prefix . "usermeta " . "<BR><BR>";
     	
    echo "Cimy Plugin (not required): " . $dynamic_ud_cimy_installed . "<BR><BR>";
     	    	
	echo "Sort Field: " . $dud_options['user_directory_sort'] . "<BR>"; 
	
	$sort_order_items = dynamic_ud_sort_order( $dud_options['user_directory_sort_order'] );
	
	echo "<BR>Sort Order:<BR><BR>";
		foreach($sort_order_items as $sort_item) {
			echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $sort_item. "<BR>";
		}
		
	echo "<BR>Include/Exclude: " . $dud_options['ud_exclude_include_radio'] . "<BR>"; 
	
	$ud_hide_roles_array = $dud_options['ud_hide_roles'];
	$ud_uids_array = $dud_options['ud_users_exclude_include'];
	
	if($ud_uids_array)
		echo "<BR>Size of Include/Exclude UIDs Array: " . sizeof($ud_uids_array) . "<BR>";
	else
		echo "<BR>UIDs Selected for Include/Exclude: none<BR>";
		
	
	if($ud_hide_roles_array)
	{
		echo "<BR>Roles selected for hiding:<BR><BR>";
		foreach($ud_hide_roles_array as $ud_role)
			echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $ud_role . "<BR>";
	}
	else
		echo "<BR>Roles selected for hiding: none<BR>";
		
	echo "<BR>Show avatars: " . $dud_options['user_directory_show_avatars'] . "<BR>";     
    echo "<BR>Avatar Style: " . $dud_options['user_directory_avatar_style'] . "<BR>";     
	echo "<BR>Border: " . $dud_options['user_directory_border'] . "<BR>";
	echo "<BR>Border Len: " . $dud_options['user_directory_border_length'] . "<BR>";
	echo "<BR>Border Style: " . $dud_options['user_directory_border_style'] . "<BR>";
	echo "<BR>Border Color: " . $dud_options['user_directory_border_color'] . "<BR>";
	echo "<BR>Border Thickness: " . $dud_options['user_directory_border_thickness'] . "<BR>";
	echo "<BR>Directory Font Size: " . $dud_options['user_directory_listing_fs'] . "<BR>";
	echo "<BR>Directory Listing Spacing: " . $dud_options['user_directory_listing_spacing'] . "<BR>";
	echo "<BR>Link to Author Page: " . $dud_options['ud_author_page'] . "<BR>";
	echo "<BR>Author Page Target Window: " . $dud_options['ud_target_window'] . "<BR>";
	
	echo "</PRE>";
}

