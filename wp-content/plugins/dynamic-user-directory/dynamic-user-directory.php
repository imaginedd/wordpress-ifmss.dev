<?php
/*** Plugin Name: Dynamic User Directory 
* Plugin URI: http://sgcustomwebsolutions.com 
* Description: Creates an alphabetically sorted user directory that will format and display specified user meta data such as name, address, and email. 
* Version: 1.2.7 
* Author: Sarah Giles 
* Author URI: http://sgcustomwebsolutions.com 
* License: GPL2 
*/

define('DYNAMIC_USER_DIRECTORY_DIR_PATH', plugin_dir_path(__FILE__));
define('DYNAMIC_USER_DIRECTORY_URL', plugin_dir_url(__FILE__));

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'my_plugin_action_links' );

function my_plugin_action_links( $links ) {
  
   $links[] = '<a href="http://sgcustomwebsolutions.com/wordpress-plugin-development/" target="_blank">Add-ons</a>';
   return $links;
}

/*** DEFINE CIMY CONSTANTS IF CIMY USER EXTRA FIELDS PLUGIN IS INSTALLED ***/
global $wpdb;

$table_name1 = "wp_cimy_uef_data";
$table_name2 = "wp_cimy_uef_fields";

$dudCimyTable1 = $wpdb->get_results("SHOW TABLES LIKE '" . $table_name1 . "'");
$dudCimyTable2 = $wpdb->get_results("SHOW TABLES LIKE '" . $table_name2 . "'");

if(!$dudCimyTable1 && !$dudCimyTable2)
{	
	$table_name1 = $wpdb->prefix . 'cimy_uef_data';	
	$table_name2 = $wpdb->prefix . 'cimy_uef_fields';			
	$dudCimyTable1 = $wpdb->get_results("SHOW TABLES LIKE '" . $table_name1 . "'");	
	$dudCimyTable2 = $wpdb->get_results("SHOW TABLES LIKE '" . $table_name2 . "'");
}
	
if($dudCimyTable1 && $dudCimyTable2)
{	
		if(!defined("DUD_CIMY_DATA_TABLE"))		
			define('DUD_CIMY_DATA_TABLE', $table_name1);					
		if(!defined("DUD_CIMY_FIELDS_TABLE"))		
			define('DUD_CIMY_FIELDS_TABLE', $table_name2);	
}
		
function DynamicUserDirectoryLoad()
{		    
			if(is_admin()) //load admin files only in admin  
			require_once(DYNAMIC_USER_DIRECTORY_DIR_PATH . 'includes/admin.php');            
			require_once(DYNAMIC_USER_DIRECTORY_DIR_PATH . 'includes/core.php'); 			
}

DynamicUserDirectoryLoad();